package com.muhardin.endy.latihan.java.oop;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PembelianDetail {
    private Pembelian pembelian;
    private Produk produk;
    private Integer jumlah;

    public BigDecimal subtotal(){
        return produk.getHarga().multiply(new BigDecimal(jumlah));
    }
}
