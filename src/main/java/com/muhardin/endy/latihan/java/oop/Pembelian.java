package com.muhardin.endy.latihan.java.oop;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Pembelian {
    private LocalDateTime waktuTransaksi = LocalDateTime.now();
    private Customer customer;
    private List<PembelianDetail> daftarPembelianDetail = new ArrayList<>();
    private List<Diskon> daftarDiskon = new ArrayList<>();

    public BigDecimal total(){
        BigDecimal nilaiTotal = BigDecimal.ZERO;
        
        for (PembelianDetail pembelianDetail : daftarPembelianDetail) {
            nilaiTotal = nilaiTotal.add(pembelianDetail.subtotal());
        }

        return nilaiTotal;
    }

    public BigDecimal totalDiskon(){
        BigDecimal nilaiTotal = BigDecimal.ZERO;

        for (Diskon diskon : daftarDiskon) {
            nilaiTotal = nilaiTotal.add(diskon.hitung(this));
        }

        return nilaiTotal;
    }

    public void cetakStruk(){
        System.out.println("Waktu transaksi : "+waktuTransaksi.toString());
        System.out.println("----------------------------------");

        for(PembelianDetail pd : daftarPembelianDetail){
            System.out.println(pd.getProduk().getNama());
            System.out.print("          " + pd.getJumlah() + "    x    "+pd.getProduk().getHarga());
            System.out.println("            : "+pd.subtotal());
            System.out.println();
        }

        System.out.println("----------------------------------------------------");

        for(Diskon d : daftarDiskon){
            System.out.println(d.getClass().getSimpleName() + " : " + d.hitung(this));
        }

        System.out.println("----------------------------------------------------");
        System.out.println("Total                                   : "+total().setScale(2, RoundingMode.HALF_EVEN));
        System.out.println("Total diskon                            : "+totalDiskon());
        System.out.println("Nilai Pembayaran                        : "+ total().subtract(totalDiskon()));
    }
}
