package com.muhardin.endy.latihan.java.hr;

import java.time.LocalDate;

public class Karyawan {

    public static Integer jumlahKaryawan = 0;

    public String nip;
    public String nama;
    private LocalDate tanggalLahir;
    private LocalDate tanggalBergabung;

    public Karyawan(){
        jumlahKaryawan = jumlahKaryawan + 1;
        System.out.println("Membuat karyawan baru");
    }

    public Karyawan(String nip, String nama){
        this();
        this.nip = nip;
        this.nama = nama;
    }

    public LocalDate getTanggalLahir() {
        // query dulu ke db
        return tanggalLahir;
    }
    public void setTanggalLahir(LocalDate x) {
        // insert audit log
        tanggalLahir = x;
    }

    public Integer hitungUsia(){
        Integer usia = LocalDate.now().getYear() - tanggalLahir.getYear();
        return usia;
    }
}
