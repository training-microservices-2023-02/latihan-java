package com.muhardin.endy.latihan.java.oop;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Produk {
    private String kode;
    private String nama;
    private BigDecimal harga;
}
