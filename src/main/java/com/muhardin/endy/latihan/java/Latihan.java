package com.muhardin.endy.latihan.java;

import java.time.LocalDate;

import com.muhardin.endy.belajar.Kalkulator;
import com.muhardin.endy.latihan.java.hr.Karyawan;

public class Latihan {
    public static void main(String[] args) {
        Kalkulator k1 = new Kalkulator();
        Integer hasil = k1.tambah(4,18);
        System.out.println("Hasil penjumlahan = " + hasil );

        System.out.println("Jumlah karyawan : " + Karyawan.jumlahKaryawan);

        Karyawan k = new Karyawan();
        k.nama = "Endy";
        // k.tanggalLahir = LocalDate.of(2000, 12, 31);
        k.setTanggalLahir(LocalDate.of(2000, 12, 31));
        System.out.println("Nama k : "+k.nama);
        System.out.println("Jumlah karyawan k : "+k.jumlahKaryawan);
        System.out.println("Usia k : "+k.hitungUsia());

        Karyawan k2 = new Karyawan("123", "Maris");
        System.out.println("Nama k2 : "+k2.nama);
        System.out.println("Jumlah karyawan k2 : "+k2.jumlahKaryawan);

        // tampilkan isi k sekali lagi
        System.out.println("Nama k : "+k.nama);
        System.out.println("Jumlah karyawan k : "+k.jumlahKaryawan);

        System.out.println("Jumlah karyawan : " + Karyawan.jumlahKaryawan);
    }

}
