package com.muhardin.endy.latihan.java.oop;

import java.math.BigDecimal;
import java.time.LocalDate;


public class DiskonPeriode implements Diskon {

    private BigDecimal persentase;
    private LocalDate mulai;
    private LocalDate sampai;

    public DiskonPeriode(LocalDate m, LocalDate s, BigDecimal p){
        mulai = m;
        sampai = s;
        persentase = p;
    }

    public BigDecimal hitung(Pembelian p){
        
        if(p.getWaktuTransaksi().toLocalDate().isAfter(mulai) 
            && p.getWaktuTransaksi().toLocalDate().isBefore(sampai)) {
             return p.total().multiply(persentase);   
        }

        return BigDecimal.ZERO;
    }
}
