package com.muhardin.endy.latihan.java.oop;

import java.math.BigDecimal;

public interface Diskon {
    public BigDecimal hitung(Pembelian p);
}
