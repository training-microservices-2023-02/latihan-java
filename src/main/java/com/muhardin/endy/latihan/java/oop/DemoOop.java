package com.muhardin.endy.latihan.java.oop;

import java.math.BigDecimal;
import java.time.LocalDate;

public class DemoOop {
    public static void main(String[] args) {
        Produk p1 = new Produk();
        p1.setKode("P-001");
        p1.setNama("Laptop Asus");
        p1.setHarga(new BigDecimal("10000000"));

        Produk p2 = new Produk();
        p2.setKode("P-002");
        p2.setNama("Laptop Lenovo");
        p2.setHarga(new BigDecimal("12000000"));

        Produk p3 = new Produk();
        p3.setKode("P-003");
        p3.setNama("Laptop HP");
        p3.setHarga(new BigDecimal("13000000"));

        Customer c1 = new Customer();
        c1.setNama("Endy");

        Pembelian pembelian = new Pembelian();
        pembelian.setCustomer(c1);
        
        PembelianDetail pd1 = new PembelianDetail();
        pd1.setPembelian(pembelian);
        pd1.setProduk(p3);
        pd1.setJumlah(1);
        pembelian.getDaftarPembelianDetail().add(pd1);

        PembelianDetail pd2 = new PembelianDetail();
        pd2.setPembelian(pembelian);
        pd2.setProduk(p1);
        pd2.setJumlah(1);
        pembelian.getDaftarPembelianDetail().add(pd2);

        DiskonTotal d1 = new DiskonTotal();
        d1.setPersentase(new BigDecimal("0.1")); // setter belum tentu dipanggil 
        d1.setPembelianMinimal(new BigDecimal("15000000"));
        pembelian.getDaftarDiskon().add(d1);

        DiskonPeriode d2 = new DiskonPeriode(LocalDate.of(2023, 8, 1), 
            LocalDate.of(2023, 10, 1), new BigDecimal("0.05"));
        
        pembelian.getDaftarDiskon().add(d2);
        
        pembelian.cetakStruk();
    }
}
