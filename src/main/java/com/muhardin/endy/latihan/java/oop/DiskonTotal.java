package com.muhardin.endy.latihan.java.oop;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DiskonTotal implements Diskon {
    private BigDecimal pembelianMinimal; // nilainya null, maka harus diisi dengan setter
    private BigDecimal persentase;

    @Override
    public BigDecimal hitung(Pembelian p) {
        BigDecimal diskon = BigDecimal.ZERO;
        
        // kalau total < atau = batas minimal, tidak dapat diskon
        if(p.total().compareTo(pembelianMinimal) < 1) {
            return diskon;
        }

        return p.total().multiply(persentase);
    }
}
